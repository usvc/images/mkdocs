IMAGE_URL=usvc/mkdocs
IMAGE_TIMESTAMP=$$(date +'%Y%m%d')

run: build_material
	docker run \
		--volume $$(pwd)/example:/example \
		--workdir /example \
		--publish 8000:8000 \
		$(IMAGE_URL):material-latest \
		serve \
			--livereload \
			--dev-addr 0.0.0.0:8000

build:
	$(MAKE) build_base
	$(MAKE) build_material
	$(MAKE) build_gitbook
	$(MAKE) build_bootstrap386
build_base:
	$(MAKE) _build TARGET=base
build_material:
	$(MAKE) _build TAG_PREFIX="material-" TARGET=material
build_gitbook:
	$(MAKE) _build TAG_PREFIX="gitbook-" TARGET=gitbook
build_bootstrap386:
	$(MAKE) _build TAG_PREFIX="bootstrap386-" TARGET=bootstrap386
_build:
	docker build \
		--target ${TARGET} \
		--tag $(IMAGE_URL):${TAG_PREFIX}latest .

ci.export:
	$(MAKE) ci.export_base
	$(MAKE) ci.export_material
	$(MAKE) ci.export_gitbook
	$(MAKE) ci.export_bootstrap386
ci.export_base:
	mkdir -p ./.export
	docker save --output ./.export/base.tar $(IMAGE_URL):latest
ci.export_material:
	mkdir -p ./.export
	docker save --output ./.export/material.tar $(IMAGE_URL):material-latest
ci.export_gitbook:
	mkdir -p ./.export
	docker save --output ./.export/gitbook.tar $(IMAGE_URL):gitbook-latest
ci.export_bootstrap386:
	mkdir -p ./.export
	docker save --output ./.export/bootstrap386.tar $(IMAGE_URL):bootstrap386-latest

ci.import:
	-$(MAKE) ci.import_base
	-$(MAKE) ci.import_material
	-$(MAKE) ci.import_gitbook
	-$(MAKE) ci.import_bootstrap386
ci.import_base:
	docker load --input ./.export/base.tar
ci.import_material:
	docker load --input ./.export/material.tar
ci.import_gitbook:
	docker load --input ./.export/gitbook.tar
ci.import_bootstrap386:
	docker load --input ./.export/bootstrap386.tar

publish:
	$(MAKE) publish_base
	$(MAKE) publish_material
	$(MAKE) publish_gitbook
	$(MAKE) publish_bootstrap386
publish_base: build_base
	$(MAKE) _publish TAG_PREFIX= TAG=$(IMAGE_TIMESTAMP)
publish_material: build_material
	$(MAKE) _publish TAG_PREFIX="material-" TAG=$(IMAGE_TIMESTAMP)
publish_gitbook: build_gitbook
	$(MAKE) _publish TAG_PREFIX="gitbook-" TAG=$(IMAGE_TIMESTAMP)
publish_bootstrap386: build_bootstrap386
	$(MAKE) _publish TAG_PREFIX="bootstrap386-" TAG=$(IMAGE_TIMESTAMP)
_publish:
	docker tag $(IMAGE_URL):${TAG_PREFIX}latest $(IMAGE_URL):${TAG_PREFIX}${TAG}
	docker push $(IMAGE_URL):${TAG_PREFIX}latest
	docker push $(IMAGE_URL):${TAG_PREFIX}${TAG}
