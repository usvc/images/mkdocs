FROM alpine:latest AS base
RUN wget -O /usr/bin/alpine-bootstrap.sh https://gitlab.com/usvc/images/ci/base/raw/master/shared/alpine-bootstrap.sh \
  && chmod +x /usr/bin/alpine-bootstrap.sh \
  && /usr/bin/alpine-bootstrap.sh \
  && rm -rf /usr/bin/alpine-bootstrap.sh
RUN apk add --no-cache python3
RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache-dir mkdocs
LABEL \
  description="a base mkdocs image" \
  canonical_url="https://gitlab.com/usvc/images/mkdocs" \
  license="MIT" \
  maintainer="zephinzer" \
  authors="zephinzer"

FROM base AS material
RUN pip3 install --no-cache-dir mkdocs-material pygments
LABEL \
  theme="material" \
  theme_url="https://github.com/squidfunk/mkdocs-material"

FROM base AS gitbook
RUN pip3 install --no-cache-dir mkdocs-gitbook pygments
LABEL \
  theme="gitbook" \
  theme_url="https://gitlab.com/lramage/mkdocs-gitbook-theme"

FROM base AS bootstrap386
RUN pip3 install --no-cache-dir mkdocs-bootstrap386 pygments
LABEL  \
  theme="bootstrap386" \
  theme_url="https://gitlab.com/lramage/mkdocs-bootstrap386"
