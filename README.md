# `usvc/mkdocs`

[![pipeline status](https://gitlab.com/usvc/images/mkdocs/badges/master/pipeline.svg)](https://gitlab.com/usvc/images/mkdocs/commits/master)
[![dockerhub link](https://img.shields.io/badge/dockerhub-usvc%2Fmkdocs-blue)](https://hub.docker.com/r/usvc/mkdocs)

This is repository contains the Dockerfile required to create the `usvc/mkdocs` image.

# Usage

## Pull the Image

```sh
docker pull usvc/mkdocs:latest
```

## Run the Image

Ensure there is a `mkdocs.yml` file and a `./docs` directory in your current directory, and run:

```sh
docker run \
  --volume $(pwd):/app \
  --workdir /app \
  --publish 8000:8000 \
  usvc/mkdocs:latest \
    serve \
      --livereload \
      --dev-addr 0.0.0.0:8000
```

> See [./example](./example) for an example.

## Customising the Image

### Material

To enable CodeHilite, add the following lines to your `mkdocs.yml`:

```yaml
# ...
markdown_extensions:
  - codehilite
# ...
```

# Development Runbook

## CI/CD Variables

| Key | Variable |
| --- | --- |
| `DOCKERHUB_USERNAME` | Username for DockerHub |
| `DOCKERHUB_PASSWORD` | Password for DockerHub |

# License

This code is licensed under [the MIT license](./LICENSE).
